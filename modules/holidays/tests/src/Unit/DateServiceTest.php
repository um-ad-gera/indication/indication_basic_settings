<?php

namespace Drupal\holidays\Tests\DateService\Unit;

require '../../../src/DateService.php';

use Drupal\holidays\DateService;
use Exception;
use \PHPUnit\Framework\TestCase;
use DateTime;

/**
 * Tests the DateService class methods.
 *
 * @group holidays
 */
class DateServiceTest extends TestCase {

  protected $getCurrentTime;

  /**
   * {@inheritdoc}
   *
   */
  protected function setUp() : void
  {
    parent::setUp();
    $this->getCurrentTime = new DateService('2021-01-01');
  }

  /**
   * Tests the holidays::getCurrentTime() method.
   * This method is tested for the current time as 2021-01-01,
   * since it will not give precise correct current time per millisecond when tested.
   */
  public function testGetCurrentTime()
  {
    $this->assertEquals(new DateService('2021-01-01'), $this->getCurrentTime->getCurrentTime());
    $this->assertNotEquals(new DateService('2021-01-02'), $this->getCurrentTime->getCurrentTime());
  }

  /**
   * Tests the holidays::isItAWeekend() method.
   */
  public function testIsItAWeekend()
  {
    $isItAWeekend = new DateService();
    $this->assertEquals(false, $isItAWeekend->isItAWeekend(new DateTime('2021-03-23')));
    $this->assertNotEquals(true, $isItAWeekend->isItAWeekend(new DateTime('2021-03-23')));
    $this->assertEquals(true, $isItAWeekend->isItAWeekend(new DateTime('2021-03-21')));
    $this->assertNotEquals(false, $isItAWeekend->isItAWeekend(new DateTime('2021-03-21')));
  }

  /**
   * Tests the holidays::getEasterSunday() method.
   * @throws Exception
   */
  public function testGetEasterSunday()
  {
    $getEasterSunday = new DateService();
    $this->assertEquals('2021-04-04', $getEasterSunday->getEasterSunday(new DateTime('2021-01-01'))->format('Y-m-d'));
    $this->assertNotEquals('2021-03-04', $getEasterSunday->getEasterSunday(new DateTime('2021-01-01'))->format('Y-m-d'));
  }

  /**
   * Tests the holidays:getFirstDayOfSummer() method.
   * @throws Exception
   */
  public function testGetFirstDayOfSummer()
  {
    $getFirstDayOfSummer = new DateService();
    $this->assertEquals('2021-04-22', $getFirstDayOfSummer->getFirstDayOfSummer(new DateTime('2021-01-01'))->format('Y-m-d'));
    $this->assertNotEquals('2021-03-22', $getFirstDayOfSummer->getFirstDayOfSummer(new DateTime('2021-01-01'))->format('Y-m-d'));
  }

  /**
   * Tests the holidays:getMerchantHoliday() method.
   * @throws Exception
   */
  public function testGetMerchantHoliday()
  {
    $getMerchantHoliday = new DateService();
    $this->assertEquals('2021-08-02', $getMerchantHoliday->getMerchantHoliday(new DateTime('2021-01-01'))->format('Y-m-d'));
    $this->assertNotEquals('2021-08-01', $getMerchantHoliday->getMerchantHoliday(new DateTime('2021-01-01'))->format('Y-m-d'));
  }

  /**
   * Tests the holidays:getAllHolidaysOverAYear() method.
   * @throws Exception
   */
  public function testGetAllHolidaysOverAYear()
  {
    $getAllHolidaysOverAYear = new DateService();
    $this->assertEquals([
      'new_years_day'=> new DateTime('2021-01-01'),
      'may_first' => new DateTime('2021-05-01'),
      'june_seventeenth' => new DateTime('2021-06-17'),
      'christmas_eve' => new DateTime('2021-12-24'),
      'christmas_day' => new DateTime('2021-12-25'),
      'second_of_xmas' => new DateTime('2021-12-26'),
      'new_years_eve' => new DateTime('2021-12-31'),
      'maundy_thursday' => new DateTime('2021-04-01'),
      'great_friday' => new DateTime('2021-04-02'),
      'easter_sunday' => new DateTime('2021-04-04'),
      'easter_monday' => new DateTime('2021-04-05'),
      'ascension_of_jesus' => new DateTime('2021-05-13'),
      'pentecost' => new DateTime('2021-05-23'),
      'whit_monday' => new DateTime('2021-05-24'),
      'first_day_of_summer' => new DateTime('2021-04-22'),
      'merchant_holiday' => new DateTime('2021-08-02')
    ], $getAllHolidaysOverAYear->getAllHolidaysOverAYear(new DateTime('2021-01-01')));
    $this->assertNotEquals([
      'new_years_day'=> new DateTime('2021-01-02'),
      'may_first' => new DateTime('2021-05-02'),
      'june_seventeenth' => new DateTime('2021-06-18'),
      'christmas_eve' => new DateTime('2021-12-25'),
      'christmas_day' => new DateTime('2021-12-27'),
      'second_of_xmas' => new DateTime('2021-12-27'),
      'new_years_eve' => new DateTime('2021-12-30'),
      'maundy_thursday' => new DateTime('2021-04-02'),
      'great_friday' => new DateTime('2021-04-03'),
      'easter_sunday' => new DateTime('2021-04-04'),
      'easter_monday' => new DateTime('2021-04-06'),
      'ascension_of_jesus' => new DateTime('2021-05-14'),
      'pentecost' => new DateTime('2021-05-24'),
      'whit_monday' => new DateTime('2021-05-25'),
      'first_day_of_summer' => new DateTime('2021-04-23'),
      'merchant_holiday' => new DateTime('2021-08-03')
    ], $getAllHolidaysOverAYear->getAllHolidaysOverAYear(new DateTime('2021-01-01')));
  }
}
