<?php
namespace Drupal\holidays\Commands;
use DateTime;
use Drupal;
use Drupal\holidays\DateService;
use Drush\Commands\DrushCommands;
use Exception;

/**
 * A Drush command file.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 * - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 * - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class HolidaysCommands extends DrushCommands {
  /**
   * @command holidays:test_time
   * @aliases test_time
   * @usage holidays:test_time
   *   We are testing the time here!
   */
  public function testGetCurrentTime() {
    /** @var DateService $service */
    $service = Drupal::service('holidays.date_service');
    $time = $service->getCurrentTime();
    echo $time->format('w');
  }
  /**
   * @command holidays:test_isitaweekend
   * @aliases isitaweekend
   * @usage holidays:isitaweekend
   *   We are testing if it's a weekend!
   */
  public function testIsItAWeekend() {
    /** @var DateService $service */
    $service = $service = Drupal::service('holidays.date_service');
    $is_it_a_weekend = $service->isItAWeekend(new DateTime('2021-03-20-08:20'));
    $a = 10;
  }

  /**
   * @command holidays:test_geteastersunday
   * @aliases geteastersunday
   * @usage holidays:geteastersunday
   *   We are calculating date for easter sunday!
   * @throws Exception
   */
  public function testGetEasterSunday() {
    /** @var DateService $service */
    $service = $service = Drupal::service('holidays.date_service');
    $easter_sunday = $service->getEasterSunday(new DateTime('now'));
    $a = 10;
  }

  /**
   * @command holidays:test_getfirstdayofsummer
   * @aliases getfirstdayofsummer
   * @usage holidays:getfirstdayofsummer
   *   We are calculating date for first day of summer!
   * @throws Exception
   */
  public function testGetFirstDayOfSummer() {
    /** @var DateService $service */
    $service = $service = Drupal::service('holidays.date_service');
    $first_day_of_summer = $service->getFirstDayOfSummer(new DateTime('now'));
    $a = 10;
  }

  /**
   * @command holidays:test_getmerchantholiday
   * @aliases getmerchantholiday
   * @usage holidays:getmerchantholiday
   *   We are calculating date for merchant holiday!
   * @throws Exception
   */
  public function testGetMerchantHoliday() {
    /** @var DateService $service */
    $service = $service = Drupal::service('holidays.date_service');
    $merchant_holiday = $service->getMerchantHoliday(new DateTime('now'));
    $a = 10;
  }

  /**
   * @command holidays:test_getallholidaysoverayear
   * @aliases getallholidaysoverayear
   * @usage holidays:getallholidaysoverayear
   *   We are calculating dates for all holidays over a year!
   * @throws Exception
   */
  public function testGetAllHolidaysOverAYear() {
    /** @var DateService $service */
    $service = $service = Drupal::service('holidays.date_service');
    $all_holidays_over_a_year = $service->getAllHolidaysOverAYear(new DateTime('2015-01-01'));
    $a = 10;
  }
}
