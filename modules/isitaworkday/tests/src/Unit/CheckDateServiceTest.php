<?php

require "../../../src/CheckDateService.php";
require "../../../../holidays/src/DateService.php";

use Drupal\holidays\DateService;
use Drupal\isitaworkday\CheckDateService;
use \PHPUnit\Framework\TestCase;

/**
 * Tests the CheckDateService class methods.
 *
 * @group isitaworkday
 */
class CheckDateServiceTest extends TestCase {

  /**
   * Tests the holidays::isItAWorkday() method.
   * @throws Exception
   */
  public function testIsItAWorkday()
  {
    $isItAWorkday = new CheckDateService();
    $this->assertEquals(false, $isItAWorkday->isItAWorkday(new DateService('2021-03-21')));
    $this->assertNotEquals(true, $isItAWorkday->isItAWorkday(new DateService('2021-03-21')));
  }

  /**
   * Tests the holidays:isItAHoliday() method.
   * @throws Exception
   */
  public function testIsItAHoliday()
  {
    $isItAHoliday = new CheckDateService();
    $this->assertEquals(true, $isItAHoliday->isItAHoliday(new DateService('2021-12-24')));
    $this->assertNotEquals(false, $isItAHoliday->isItAHoliday(new DateService('2021-12-24')));
  }

  /**
   * Tests the holidays:getLastResponseDate() method.
   * @throws Exception
   */
  public function testGetLastResponseDate()
  {
    $getLastResponseDate = new CheckDateService();
    $this->assertEquals('2021-01-06-08:20', $getLastResponseDate->getLastResponseDate(new DateService('2021-01-01' . '17:15'), '2')->format('Y-m-d-h:i'));
    $this->assertNotEquals('2021-01-05-08:20', $getLastResponseDate->getLastResponseDate(new DateService('2021-01-01' . '17:15'), '2')->format('Y-m-d-h:i'));
    $this->assertEquals('2021-03-24-12:00', $getLastResponseDate->getLastResponseDate(new DateService('2021-03-21' . '12:00'), '2')->format('Y-m-d-h:i'));
    $this->assertNotEquals('2021-03-25-12:00', $getLastResponseDate->getLastResponseDate(new DateService('2021-03-21' . '12:00'), '2')->format('Y-m-d:i'));
    $this->assertEquals('2021-03-24-11:20', $getLastResponseDate->getLastResponseDate(new DateService('2021-03-22' . '11:20'), '2')->format('Y-m-d-h:i'));
    $this->assertNotEquals('2021-03-25-11:20', $getLastResponseDate->getLastResponseDate(new DateService('2021-03-22' . '11:20'), '2')->format('Y-m-d-h:i'));
    $this->assertEquals('2021-03-25-12:00', $getLastResponseDate->getLastResponseDate(new DateService('2021-03-22' . '12:00'), '3')->format('Y-m-d-h:i'));
    $this->assertNotEquals('2021-03-26-12:00', $getLastResponseDate->getLastResponseDate(new DateService('2021-03-22' . '12:00'), '2')->format('Y-m-d-h:i'));
  }
}
