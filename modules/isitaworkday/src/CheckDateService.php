<?php

namespace Drupal\isitaworkday;

use \Drupal\holidays\DateService;
use DateTime;
use Exception;

/**
 * Class CheckDateService.
 */
class CheckDateService {
  private $holidays;

  public function __construct(DateService $holidays) {
    $this->holidays = $holidays;
  }

  /**
   * Checking whether a given date is a workday or not --> from holidays module/DateService
   * @param DateTime $datetime
   * @return bool
   * @throws Exception
   */
  public function isItAWorkday(DateTime $datetime): bool {
    $holidays = $this->holidays->getAllHolidaysOverAYear($datetime);
    foreach($holidays as $holiday) {
      if($holiday == $datetime) {
        return false;
      }
    }
    if($this->holidays->isItAWeekend($datetime)) {
      return false;
    }
    return true;
  }

  /**
   * Checking whether a given date is a holiday or not --> from holidays module/DateService
   * @param DateTime $datetime
   * @return bool
   * @throws Exception
   */
  public function isItAHoliday(DateTime $datetime): bool {
    $holidays = $this->holidays->getAllHolidaysOverAYear($datetime);
    foreach($holidays as $holiday) {
      if($holiday == $datetime) {
        return true;
      }
    }
    if($this->holidays->isItAWeekend($datetime)) {
      return true;
    }
    return false;
  }

  /**
   * Getting last response date
   * @param DateTime $day
   * @param int $days_to_respond
   * @return DateTime
   * @throws Exception
   */
  public function getLastResponseDate(DateTime $day, $days_to_respond = 2, $hours_of_operations = null )  : DateTime {
    $last_response_date = $day;
    if(!$hours_of_operations) {
      $hours_of_operations['opening_hour'] = 8;
      $hours_of_operations['opening_minute'] = 20;
      $hours_of_operations['closing_hour'] = 16;
      $hours_of_operations['closing_minute'] = 20;
      $hours_of_operations['opening_string'] = str_pad($hours_of_operations['opening_hour'] . ':' . $hours_of_operations['opening_minute'], STR_PAD_LEFT);
      $hours_of_operations['closing_string'] = str_pad($hours_of_operations['closing_hour'] . ':' . $hours_of_operations['closing_minute'], STR_PAD_LEFT);
    }

    /**
     * Check to see if the service desk is open (hour wise).
     * If it isn't, then add one day to the response time.
     */
    $opening_hour = new DateTime($last_response_date->format('Y-M-d' . $hours_of_operations['opening_string']));
    $closing_hour = new DateTime($last_response_date->format('Y-M-d' . $hours_of_operations['closing_string']));
    $dateVal = $last_response_date->diff($opening_hour);
    $closingVal = $last_response_date->diff($closing_hour);

    if($closingVal->h <= 0) {
      if($closingVal->i > 0)
      $last_response_date->modify('+1 day');
      $last_response_date->setTime($hours_of_operations['opening_hour'],$hours_of_operations['opening_minute']);
    }
    elseif($dateVal->h < 0 || $dateVal->invert == 0) {
      $last_response_date->setTime($hours_of_operations['opening_hour'],$hours_of_operations['opening_minute']);
    }

    /**
     * Check to see if the day is a holiday.
     * If it is, add one day to the response time.
     */
    if($this->isItAHoliday($last_response_date)) {
      $last_response_date->modify('+1 day');
      $last_response_date->setTime($hours_of_operations['opening_hour'],$hours_of_operations['opening_minute']);
    }

    /**
     * While we still have some response time left, check to see if the day is a holiday or not.
     * We always add one day to the response,
     * but if it wasn't a holiday, we deduct one day from the total days to respond.
     */
    while($days_to_respond > 0) {
      if($this->isItAHoliday($last_response_date)) {
        $last_response_date->modify('+1 day');

      }
      else {
        $last_response_date->modify("+1 day");
        $days_to_respond--;
      }
    }

    /**
     * After we have deducted all the response days, we still have to check if that day is a holiday or not.
     * Therefore we have to go over the holidays once again.
     */
    while($this->isItAHoliday($last_response_date)) {
      $last_response_date->modify('+1 day');
    }
    return $last_response_date;
  }
}
