<?php
namespace Drupal\indication\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ServiceDeskConfigurationForm extends ConfigFormBase {

  protected function getEditableConfigNames() {
    return ['indication.service_desk_configuration'];
  }

  public function getFormId() {
    return 'service_desk_configuration_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('indication.service_desk_configuration');

    $form['service_desk_name'] = [
      '#type' => 'textfield',
      '#title' => t('Service desk name'),
      '#description' => t('The "name" of the service desk, as defined in "Ábendingategundir". Since each municipality can have different names for their departments, we need to be able to change the name of the department usually called "Service desk". If your department\'s name is something else, please enter it here.'),
      '#default_value' => ($config->get('service_desk_name')) ? $config->get('service_desk_name') : "Þjónustuver"
    ];

    $form['service_desk_opening_hour'] = [
      '#type' => 'textfield',
      '#title' => t('Service desk opens'),
      '#description' => t('The time service desk opens, it the format of "08:20"'),
      '#default_value' => $config->get('service_desk_opening_hour'),
    ];

    $form['service_desk_opening_minute'] = [
      '#type' => 'textfield',
      '#title' => t('Service desk opens (minute)'),
      '#description' => 'The "minute" part of when the service desk opens (like 20, 25, 30)',
      '#default_value' => $config->get('service_desk_opening_minute'),
    ];

    $form['service_desk_closing_hour'] = [
      '#type' => 'textfield',
      '#title' => t('Service desk closes (hour)'),
      '#description' => t('The "hour" part of when the service desk closes (like 14, 15, 16) (24hr clock)'),
      '#default_value' => $config->get('service_desk_closing_hour'),
    ];

    $form['service_desk_closing_minute'] = [
      '#type' => 'textfield',
      '#title' => t('Service desk closes (minute)'),
      '#description' => 'The "minute" part of when the service desk closes (like 20, 25, 30)',
      '#default_value' => $config->get('service_desk_closing_minute'),
    ];

    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('indication.service_desk_configuration')
      ->set('service_desk_opening_hour', $form_state->getValue('service_desk_opening_hour'))
      ->save();

    $this->config('indication.service_desk_configuration')
      ->set('service_desk_opening_minute', $form_state->getValue('service_desk_opening_minute'))
      ->save();

    $this->config('indication.service_desk_configuration')
      ->set('service_desk_closing_hour', $form_state->getValue('service_desk_closing_hour'))
      ->save();

    $this->config('indication.service_desk_configuration')
      ->set('service_desk_closing_minute', $form_state->getValue('service_desk_closing_minute'))
      ->save();

    $this->config('indication.service_desk_configuration')
      ->set('service_desk_name', $form_state->getValue('service_desk_name'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
