<?php

namespace Drupal\indication;

use Drupal;
use Drupal\isitaworkday\CheckDateService;
use Drupal\Core\Config\ConfigFactoryInterface;

class IndicationProcessService {

  /**
   * @var CheckDateService $dateService
   */
  private $dateService;

  /**
   * @var ConfigFactoryInterface $configFactory
   */
  protected $configFactory;

  /**
   * @var string $serviceDeskName
   */
  protected $serviceDeskName;

  /**
   * @var array $serviceDeskHoursOfOperation
   */
  protected $serviceDeskHoursOfOperation;

  /**
   * IndicationProcessService constructor.
   *
   * @param ConfigFactoryInterface $config_factory
   * @param CheckDateService $date_service
   */
  public function __construct(ConfigFactoryInterface $config_factory, CheckDateService $date_service) {
    $this->configFactory = $config_factory;
    $this->dateService = $date_service;

    $service_desk_config = $this->configFactory->get('indication.service_desk_configuration');
    $this->serviceDeskName = $service_desk_config->get('service_desk_name');
    $this->serviceDeskHoursOfOperation['opening_hour'] = $service_desk_config->get('service_desk_opening_hour');
    $this->serviceDeskHoursOfOperation['opening_minute'] = $service_desk_config->get('service_desk_opening_minute');
    $this->serviceDeskHoursOfOperation['closing_hour'] = $service_desk_config->get('service_desk_closing_hour');
    $this->serviceDeskHoursOfOperation['closing_minute'] = $service_desk_config->get('service_desk_closing_minute');
    $this->serviceDeskHoursOfOperation['opening_string'] = str_pad($this->serviceDeskHoursOfOperation['opening_hour'] . ':' . $this->serviceDeskHoursOfOperation['opening_minute'], 5,'0',STR_PAD_LEFT);
    $this->serviceDeskHoursOfOperation['closing_string'] = str_pad($this->serviceDeskHoursOfOperation['closing_hour'] . ':' . $this->serviceDeskHoursOfOperation['closing_minute'], 5,'0',STR_PAD_LEFT);
  }

  public function processCreation(&$node, $status_array) {
    // Preprocess data
    $new_indication_tid = $status_array['Ný ábending'];
    $indication_tree = \Drupal::service('entity_type.manager')->getStorage('taxonomy_term')->loadTree('indication_types', 0, null, true);
    $indication_term_array = [];
    foreach($indication_tree as $key => $indication_term) {
      $array['id'] = $indication_term->id();
      $array['array_key'] = $key;

      $indication_term_array[$indication_term->name->value] = $array;
    }
    // Set status to New Indication
    $node->set('field_indication_status_ref', $new_indication_tid);

    // Copy the indication text into the "copy" field and into the interpered field
    $node->set('field_indication_text_copy', $node->field_indication_text->value);
    $node->set('field_interpered_indication_text', $node->field_indication_text->value);

    // Calculate response time
    $creation_date = new \DateTime(strftime('%Y-%m-%dT%H:%M:%S', $node->created->value));
    $service_desk = $indication_tree[$indication_term_array[$this->serviceDeskName]['array_key']];

    $respond_by = $this->dateService->getLastResponseDate($creation_date, $service_desk->field_indication_response_time->value, $this->serviceDeskHoursOfOperation);
    $respond_by_string = substr_replace($respond_by->format('Y-m-d H:i:s'), 'T', 10, 1);
    $node->set('field_indication_type_ref', $service_desk->id());
    $node->set('field_indication_respond_by', $respond_by_string);
  }

  public function processSendToExternalDp(&$node, $status_array) {
    $email_array = null;
    // Fetch the External Department, based on the Taxonomy
    $tid = $node->field_forward_to_external_dp->target_id;
    $external_dp = ($tid)
      ? \Drupal::service('entity_type.manager')->getStorage('taxonomy_term')->load($tid)
      : null;
    $email_addresses = $external_dp->field_ed_email->getValue();
    $email_addresses = array_merge($email_addresses, $node->field_forward_to_external_email->getValue());

    foreach($email_addresses as $email) {
      $email_array[] = $email['value'];
    }
    $email_string = implode(',', $email_array);
    // @TODO: Implement send email

    // Flag that the email has been sent
    $date = new \DateTime();
    $closed_indication_tid = $status_array['Lokið'];
    $node->set('field_indication_ext_mail_sent', TRUE);
    $node->set('field_indication_status_ref', $closed_indication_tid);
    $node->set('field_indication_responded_at', substr_replace($date->format('Y-m-d H:i:s'), 'T', 10, 1));
    $node->set('field_indication_close_by', substr_replace($date->format('Y-m-d H:i:s'), 'T', 10, 1));
    $node->set('field_indication_closed_at', substr_replace($date->format('Y-m-d H:i:s'), 'T', 10, 1));
  }

  public function processAssignIndication(&$node, $status_array, $department_array) {
    if(!isset($node->field_indication_responded_at->value)) {
      $date = new \DateTime();

      $in_progress_indication_tid = $status_array['Í vinnslu'];
      $assigned_department_tid = $node->field_indication_type_ref->target_id;
      $assigned_department = \Drupal::service('entity_type.manager')->getStorage('taxonomy_term')->load($assigned_department_tid);
      $close_by = $this->dateService->getLastResponseDate(new \DateTime(), $assigned_department->field_indication_response_time->value);
      $send_mail = $assigned_department->field_send_mail_f_new_indication->value;

      $node->set('field_indication_responded_at', substr_replace($date->format('Y-m-d H:i:s'), 'T', 10, 1));
      $node->set('field_indication_close_by', substr_replace($close_by->format('Y-m-d H:i:s'), 'T', 10, 1));
      $node->set('field_indication_status_ref', $in_progress_indication_tid);

      // @TODO: Implement Send email to aggrigation mailbox + all employees that have this indication type
    }
  }

  /**
   * House cleaning when an indication is closed.
   *
   * @param $node 
   *   The node is passed by reference, therefore it doesn't have a return value.
   * @param $status_array
   * @param $department_array
   */
  public function processCloseIndication(&$node, $status_array, $department_array) {
    if(!isset($node->field_indication_closed_at->value)) {
      $date = new \DateTime();
      $node->set('field_indication_closed_at', substr_replace($date->format('Y-m-d H:i:s'), 'T', 10, 1));
      $node->set('field_indication_type_ref', null);
    }
  }

  public function processAnswerIndication(&$node, $status_array, $department_array) {
    if(!isset($node->field_indication_close_by->value)) {
      $assigned_department_tid = $node->field_indication_type_ref->target_id;
      $assigned_department = \Drupal::service('entity_type.manager')->getStorage('taxonomy_term')->load($assigned_department_tid);
      $close_by = $this->dateService->getLastResponseDate(new \DateTime(), $assigned_department->field_indication_response_time->value);
      $node->set('field_indication_close_by', substr_replace($close_by->format('Y-m-d H:i:s'), 'T', 10, 1));
    }

    // @TODO: Implement Send email to reporter
  }

  /**
   * @return string Name of the Service desk, as configured in the backend.
   */
  public function getServiceDeskName(): string {
    return $this->serviceDeskName;
  }
}
