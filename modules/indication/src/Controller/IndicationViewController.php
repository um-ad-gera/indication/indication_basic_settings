<?php
namespace Drupal\indication\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\indication\IndicationProcessService;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for all the View displays for indications. Used to determine excactly WHAT view is displayed, based on
 * who is logged in and what department they belong to.
 */
class IndicationViewController extends ControllerBase {

  /**
   * @var \Drupal\indication\IndicationProcessService
   */
  protected $indicationService;

  /**
   * IndicationViewController constructor.
   *
   * @param IndicationProcessService $indicationProcessService
   */
  public function __construct(IndicationProcessService $indicationProcessService) {
    $this->indicationService = $indicationProcessService;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('indication.process_service')
    );
  }
  /**
   * New Indications
   *
   * Displays all indications either flagged as "New" or "Received", based on who is logged in.
   *
   * @return array
   *  The rendered view block with correct parameters.
   */
  public function newIndications() : array {
    $user = User::load(\Drupal::currentUser()->id());
    $department_array = $this->extractDepartments($user);

    if($this->serviceOrContextual($user))  {
      $view_block = views_embed_view('indications', 'new_indications_service_desk');
    }
    else {
      $view_block = views_embed_view('indications', 'new_indications_contextual', implode(',', $department_array));
    }

    return [
      'description' => [
        '#theme' => 'view_new_indications',
        '#indication_block_view' => $view_block,
      ]
    ];
  }

  /**
   * In Progress Indications
   *
   * Displays all indications flagged with "In progress", based on who is logged in
   *
   * @return array
   *  The rendered view block with correct parameters.
   */
  public function inProgressIndications() : array {
    $user = User::load(\Drupal::currentUser()->id());
    $department_array = $this->extractDepartments($user);

    if($this->serviceOrContextual($user))  {
      $view_block = views_embed_view('indications', 'in_progress_service_desk');
    }
    else {
      $view_block = views_embed_view('indications', 'in_progress_contextual', implode(',', $department_array));
    }

    return [
      'description' => [
        '#theme' => 'view_new_indications',
        '#indication_block_view' => $view_block,
      ]
    ];
  }

  public function overdoneIndications() {
    $user = User::load(\Drupal::currentUser()->id());
    $department_array = $this->extractDepartments($user);

    if($this->serviceOrContextual($user))  {
      $over_construction_time = views_embed_view('indications', 'over_construction_time');
      $over_allocation_time_service_desk = views_embed_view('indications', 'over_allocation_time_service_desk');
      $over_allocation_time_departments = views_embed_view('indications', 'over_allocation_time_departments');
    }
    else {
      $over_construction_time = views_embed_view('indications', 'over_construction_time_contextual', implode(',', $department_array));
      $over_allocation_time_departments = views_embed_view('indications', 'over_allocation_time_contextual', implode(',', $department_array));
      $over_allocation_time_service_desk = null;
    }

    return [
      [
        'description' => [
          '#theme' => 'view_new_indications',
          '#indication_block_view' => $over_construction_time,
        ]
      ],
      [
        'description' => [
          '#theme' => 'view_new_indications',
          '#indication_block_view' => $over_allocation_time_service_desk,
        ]
      ],
      [
        'description' => [
          '#theme' => 'view_new_indications',
          '#indication_block_view' => $over_allocation_time_departments,
        ]
      ]
    ];
  }

  /**
   * Indications containing delicate information
   *
   * Displays all indications flagged with "Delicate information". Only Service Manager is allowed to see these indications
   *
   * @return array
   *  The rendered view block with correct parameters.
   */
  public function delicateInformation() : array {
    $user = User::load(\Drupal::currentUser()->id());

    if($this->serviceOrContextual($user))  {
      $view_block = views_embed_view('indications', 'delicate_information');
    }

    return [
      'description' => [
        '#theme' => 'view_new_indications',
        '#indication_block_view' => $view_block,
      ]
    ];
  }

  private function extractDepartments(User $user) : array {
    $departments = $user->get('field_department')->getValue();
    $department_array = null;

    foreach($departments as $department) {
      $department_array[] = $department['target_id'];
    }

    return $department_array;
  }

  /**
   * Determines if the user is "admin" (service desk) or needs the contextual view.
   *
   * @param User $user
   * @return bool
   */
  private function serviceOrContextual(User $user) : bool {
    $roles = $user->getRoles();
    $flipped = array_flip($roles);

    return isset($flipped['administrator']) || isset($flipped['service_desk']) || isset($flipped['service_manager']);
  }
}
